Source: indi-gige
Section: science
Priority: optional
Maintainer: Debian Astronomy Team <debian-astro-maintainers@lists.alioth.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 13)
	, cmake
	, libindi-dev
	, libapogee-dev
	, libcfitsio-dev
	, zlib1g-dev
	, libaravis-dev
Standards-Version: 4.6.0
Homepage: https://github.com/indilib/indi-3rdparty
Rules-Requires-Root: no
XS-Autobuild: yes
Vcs-Browser: https://salsa.debian.org/debian-astro-team/indi-gige
Vcs-Git: https://salsa.debian.org/debian-astro-team/indi-gige.git

Package: indi-gige
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends}
	, libapogee3
Description: INDI driver for GigE machine vision cameras
 This package contains an INDI driver for most GigE machine vision cameras
 through project Aravis.
 .
 Machine vision GigE cameras are designed towards throughput, and hence
 provide emphasis on high-speed video streaming. The main design goal for
 this driver is to use these cameras as primary imager or for guiding.
 Therefore, the main goal of this driver is not to provide high-speed
 video streams, but to control these cameras more or less as a generic
 CCD camera with a manual trigger and manual exposure controls.
 .
 Many of the pre-processing features found on many of these cameras have
 therefore been not exposed.
 .
 This driver is compatible with any INDI client such as KStars or Xephem.
